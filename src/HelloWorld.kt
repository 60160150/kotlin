package org.kotlinlang.play         // 1

fun main() {                        // 2
    println("Hello, World!")        // 3
}
fun main(args: Array<String>) {
    println("Hello, World!")
}